﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin Page.aspx.cs" Inherits="Matrix_Team_Worked_Pages_and_Procedures.Admin_Page" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Page</title>
    <link rel="shortcut icon" type="image/x-icon" href="\icon.ico"/>
</head>
<body style="background-image:url(download.jpg);background-position: center center;background-repeat: no-repeat;background-attachment: fixed;background-size:cover;height:100%;width:100%;">
    <style>
        h1,h2{color:#66ff33;}
            .tblHead 
            {
                font-size: 15px;
                font-weight : bold;
                background-color: #FF6600;
                color: #FFFFFF;
                text-align : center;
            }
            .butto 
            {
                background-color: #e600ac;
                color: white;
                padding: 10px 15px;margin-left:auto;margin-right:auto;display:block;margin-bottom:0%;            
                border: none;                
                opacity: 0.9;
                font-weight:bold;            
            }
            .butto:hover
            {
                background-color: white;
                color: #e600ac;
                padding: 10px 15px;margin-left:auto;margin-right:auto;display:block;margin-bottom:0%;            
                border: none;
                cursor: pointer;            
                opacity: 0.9;
                font-weight:bold;            
            }
            .butt
            {
                background-color: #e600ac;
                color: white;
                padding: 10px 15px;
                margin-left:340px;
                border: none;                
                width:50px;            
                opacity: 0.9;
                text-decoration:none;
            }
            .butt:hover
            {
                background-color: white;
                color: #e600ac;
                padding: 10px 15px;
                margin-left:340px;
                border: none;
                cursor: pointer;
                width:50px;            
                opacity: 0.9;
                text-decoration:none;
            }
            .buttoo
            {
                background-color: #e600ac;
                color: white;
                padding: 10px 15px;
                margin-left:650px;
                border: none;                
                width:50px;            
                opacity: 0.9;
                text-decoration:none;
            }
            .buttoo:hover
            {
                background-color: white;
                color:#e600ac;
                padding: 10px 15px;
                margin-left:650px;
                border: none;
                cursor: pointer;
                width:50px;            
                opacity: 0.9;
                text-decoration:none;
            }
    </style>
    <form id="form1" runat="server">
        <div>
            <h1 align="center">Admin Page</h1>
            <h2 align="center">Task Details</h2>
            <table align="center">
                <tr>
                    <td>
                        <h2>Task No</h2>
                    </td>
                    <td>
                        <input type="text" runat="server" id="txttno" required="required" placeholder="Task Number" list="taskNo" autofocus="autofocus"/>    
                        <datalist id="taskNo" runat="server"/>      
                        <asp:Label Text="&nbsp&nbsp&nbsp&nbsp" runat="server" />      
                    </td>
                    <td>
                        <h2>Task Name</h2>
                    </td>
                    <td>
                        <input type="text" runat="server" id="txttna" placeholder="Task Name"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>Description</h2>
                    </td>
                    <td>
                        <input type="text" runat="server" id="txtd" placeholder="Task Description"/>                
                    </td>
                    <td>
                        <h2>IS Completed?</h2>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlstatus" runat="server">
                            <asp:ListItem Value="" Text="Select"/>
                            <asp:ListItem Value="yes" Text="Yes" />
                            <asp:ListItem Value="no" Text="No" />
                        </asp:DropDownList>
                    </td>
                </tr>        
            </table>
            <asp:Button ID="btn_submit" runat="server" Text="Submit" OnClick="btn_submit_Click" CssClass="butto"/><br /> <asp:LinkButton runat="server" Text="Clear" OnClick="clear_Click" CssClass="buttoo" />      
            <br /><br />
            <asp:LinkButton ID="lbtnno" OnClick="lbtnno_Click" runat="server" Text="View Not Completed" CssClass="butt"/>
            <asp:LinkButton ID="lbtnyes" OnClick="lbtnyes_Click" runat="server" CssClass="butt" Text="View Completed"/><br /><br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="row" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDeleting="GridView1_RowDeleting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" HeaderStyle-CssClass="tblHead" HorizontalAlign="Center" cellpadding="10" cellspacing="5">  
                        <Columns>  
                            <asp:BoundField DataField="row" HeaderText="row" ItemStyle-BackColor="LightCyan"/>
                            <asp:BoundField DataField="Date" HeaderText="Task Added Date" ItemStyle-BackColor="LightCyan"/>  
                            <asp:BoundField DataField="TaskNo" HeaderText="Task No" ItemStyle-BackColor="LightCyan"/>
                            <asp:BoundField DataField="TaskName" HeaderText="Task Name" ItemStyle-BackColor="LightCyan"/>
                            <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-BackColor="LightCyan"/>
                            <asp:BoundField DataField="status" HeaderText="IS Completed?" ItemStyle-BackColor="LightCyan"/>
                            <asp:BoundField DataField="Pages" HeaderText="Pages" ItemStyle-BackColor="LightCyan"/>  
                            <asp:BoundField DataField="Stored_Procedures" HeaderText="Stored_Procedures" ItemStyle-BackColor="LightCyan"/>                         
                            <asp:BoundField DataField="Tables" HeaderText="Tables" ItemStyle-BackColor="LightCyan"/>  
                            <asp:BoundField DataField="Queries" HeaderText="Queries" ItemStyle-BackColor="LightCyan"/> 
                            <asp:BoundField DataField="Rdls" HeaderText="Rdls" ItemStyle-BackColor="LightCyan"/> 
                            <asp:BoundField DataField="Functions" HeaderText="Functions" ItemStyle-BackColor="LightCyan"/>
                            <asp:BoundField DataField="Name" HeaderText="Worked By" ItemStyle-BackColor="LightCyan"/>
                            <asp:ButtonField Text="Select" CommandName="Select" ItemStyle-BackColor="LightCyan"/>
                            <asp:CommandField ShowDeleteButton="true" ItemStyle-BackColor="LightCyan"/>
                        </Columns>  
            </asp:GridView>  
            <br />
            <asp:LinkButton Text="Log Out" runat="server" CssClass="buttoo" OnClick="logout_Click" OnClientClick="Confirm()"/>         
            <script type = "text/javascript">
                function Confirm() {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    if (confirm("Are You Sure?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                    document.forms[0].appendChild(confirm_value);
                }
            </script>
        </div>
    </form>
</body>
</html>