﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;
namespace Matrix_Team_Worked_Pages_and_Procedures
{
    public partial class User_Page : System.Web.UI.Page
    {
        public static string con_string = "Data Source=192.168.2.16;Initial Catalog=karthikeyan;Persist Security Info=True;User ID=sa;Password=Sql@123";
        SqlConnection con = new SqlConnection(con_string); 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string uname = Session["Name"].ToString();
                if (!IsPostBack)
                {
                    gettaskno();
                    getpages();
                    getsps();
                    gettbl();
                    getquery();
                    getrdl();
                    getfun();
                }
            }
            catch (NullReferenceException) { Response.Write("<script>if(!alert('Please Login with your details'))document.location='Login Page.aspx';</script>"); }  
        }

        private void gettaskno()
        {
            con.Open();
            taskNo.InnerHtml = null;
            SqlCommand cmd = new SqlCommand("select TaskNo from contents where status='no'", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                taskNo.InnerHtml = taskNo.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        private void getpages()
        {
            lstp.InnerHtml = null;
            con.Open();
            SqlCommand cmd = new SqlCommand("select page from pages", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                lstp.InnerHtml = lstp.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        private void getsps()
        {
            lstsp.InnerHtml = null;
            con.Open();
            SqlCommand cmd = new SqlCommand("select sp from sps", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                lstsp.InnerHtml = lstsp.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        private void gettbl()
        {
            lstt.InnerHtml = null;
            con.Open();            
            SqlCommand cmd = new SqlCommand("select tbl from tables", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                lstt.InnerHtml = lstt.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        private void getquery()
        {
            lstq.InnerHtml = null;
            con.Open();
            SqlCommand cmd = new SqlCommand("select query from queries", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                lstq.InnerHtml = lstq.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        private void getrdl()
        {
            lstr.InnerHtml = null;
            con.Open();
            SqlCommand cmd = new SqlCommand("select rdl from rdls", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                lstr.InnerHtml = lstr.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        private void getfun()
        {
            lstf.InnerHtml = null;
            con.Open();
            SqlCommand cmd = new SqlCommand("select func from functions", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                lstf.InnerHtml = lstf.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                Session["Name"] = null;
                Response.Redirect("Login Page.aspx");
            }       
        }

        protected void tno_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                string query = "select TaskNo,TaskName,Description,Pages,Stored_Procedures,Tables,Queries,Rdls,Functions from contents where TaskNo=@tno and status='no'";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@tno", txttno.Value.ToString());
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                {
                    SqlDataReader sdr = cmd.ExecuteReader();
                    sdr.Read();
                    txttna.Value = sdr[1].ToString();
                    txtd.Value = sdr[2].ToString();
                    lbp.Value = sdr[3].ToString();
                    lbsp.Value=sdr[4].ToString();
                    lbt.Value=sdr[5].ToString();
                    lbq.Value=sdr[6].ToString();
                    lbr.Value = sdr[7].ToString();
                    lbf.Value=sdr[8].ToString();
                }
                else
                {
                    Response.Write("<script>if(!alert('Incorrect Task Number'))document.location='User Page.aspx';</script>");                    
                }
                con.Close();
            }
            catch(Exception)
            { }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            try
            {
                if (txttno.Value.ToString() != string.Empty)
                {
                    con.Open();
                    string upq = "update contents set Name=@n,Pages=@p,Stored_Procedures=@sp,Tables=@t,Queries=@q,Rdls=@r,Functions=@f where TaskNo=@tno";
                    SqlCommand upcmd = new SqlCommand(upq, con);
                    string uname = Session["Name"].ToString();
                    upcmd.Parameters.AddWithValue("@n", uname);
                    upcmd.Parameters.AddWithValue("@tno", txttno.Value.ToString());
                    upcmd.Parameters.AddWithValue("@p", lbp.Value.ToString());
                    upcmd.Parameters.AddWithValue("@sp", lbsp.Value.ToString());
                    upcmd.Parameters.AddWithValue("@t", lbt.Value.ToString());
                    upcmd.Parameters.AddWithValue("@q", lbq.Value.ToString());
                    upcmd.Parameters.AddWithValue("@r", lbr.Value.ToString());
                    upcmd.Parameters.AddWithValue("@f", lbf.Value.ToString());
                    upcmd.ExecuteNonQuery();
                    con.Close();
                    Response.Write("<script>if(!alert('Updated Successfully'))document.location='User Page.aspx';</script>");
                }
            }
            catch (Exception)
            {                
            }
        }

        protected void addp_Click(object sender, EventArgs e)
        {   
            if(txtp.Value.ToString()!="")
            {
                string getp = "select page from pages where page=@p";
                SqlCommand cmd_getp = new SqlCommand(getp, con);
                cmd_getp.Parameters.AddWithValue("@p", txtp.Value.ToString());
                con.Open();
                cmd_getp.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_getp);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count<1) 
                {
                    ds.Clear();
                    string inp = "insert into pages values(@p)";
                    SqlCommand inpc = new SqlCommand(inp, con);
                    inpc.Parameters.AddWithValue("@p", txtp.Value.ToString());
                    inpc.ExecuteNonQuery();
                    
                }
            }
            if (lbp.Value.ToString() != string.Empty)
            {
                lbp.Value = lbp.Value + ",\n" + txtp.Value.ToString();
                txtp.Value = "";
            }
            else
            {
                lbp.Value = txtp.Value.ToString();
                txtp.Value = "";
            }
            con.Close();
            getpages();
        }

        protected void addsp_Click(object sender, EventArgs e)
        {
            if (txtsp.Value.ToString() != "")
            {
                string getsp = "select sp from sps where sp=@sp";
                SqlCommand cmd_getsp = new SqlCommand(getsp, con);
                cmd_getsp.Parameters.AddWithValue("@sp", txtsp.Value.ToString());
                con.Open();
                cmd_getsp.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_getsp);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count < 1)
                {
                    ds.Clear();
                    string insp = "insert into sps values(@sp)";
                    SqlCommand inspc = new SqlCommand(insp, con);
                    inspc.Parameters.AddWithValue("@sp", txtsp.Value.ToString());
                    inspc.ExecuteNonQuery();
                    
                }
            }
            if (lbsp.Value.ToString() != string.Empty)
            {
                lbsp.Value = lbsp.Value + ",\n" + txtsp.Value.ToString();
                txtsp.Value = "";
            }
            else
            {
                lbsp.Value = txtsp.Value.ToString();
                txtsp.Value = "";
            }
            con.Close();
            getsps();
        }

        protected void addt_Click(object sender, EventArgs e)
        {
            if (txtt.Value.ToString() != "")
            {
                string gett = "select tbl from tables where tbl=@t";
                SqlCommand cmd_gett = new SqlCommand(gett, con);
                cmd_gett.Parameters.AddWithValue("@t", txtt.Value.ToString());
                con.Open();
                cmd_gett.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_gett);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count < 1)
                {
                    ds.Clear();
                    string in_t = "insert into tables values(@t)";
                    SqlCommand intc = new SqlCommand(in_t, con);
                    intc.Parameters.AddWithValue("@t", txtt.Value.ToString());
                    intc.ExecuteNonQuery();
                    
                }
            }
            if (lbt.Value.ToString() != string.Empty)
            {
                lbt.Value = lbt.Value + ",\n" + txtt.Value.ToString();
                txtt.Value = "";
            }
            else
            {
                lbt.Value = txtt.Value.ToString();
                txtt.Value = "";
            }
            con.Close();
            gettbl();
        }

        protected void addq_Click(object sender, EventArgs e)
        {
            if (txtq.Value.ToString() != "")
            {
                string getq = "select query from queries where query=@q";
                SqlCommand cmd_getq = new SqlCommand(getq, con);
                cmd_getq.Parameters.AddWithValue("@q", txtq.Value.ToString());
                con.Open();
                cmd_getq.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_getq);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count < 1)
                {
                    ds.Clear();
                    string inq = "insert into queries values(@q)";
                    SqlCommand inqc = new SqlCommand(inq, con);
                    inqc.Parameters.AddWithValue("@q", txtq.Value.ToString());
                    inqc.ExecuteNonQuery();
                    
                }
            }
            if (lbq.Value.ToString() != string.Empty)
            {
                lbq.Value = lbq.Value + ",\n" + txtq.Value.ToString();
                txtq.Value = "";
            }
            else
            {
                lbq.Value = txtq.Value.ToString();
                txtq.Value = "";
            }
            con.Close();
            getquery();
        }

        protected void addr_Click(object sender, EventArgs e)
        {
            if (txtr.Value.ToString() != "")
            {
                string getr = "select rdl from rdls where rdl=@r";
                SqlCommand cmd_getr = new SqlCommand(getr, con);
                cmd_getr.Parameters.AddWithValue("@r", txtr.Value.ToString());
                con.Open();
                cmd_getr.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_getr);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count < 1)
                {
                    ds.Clear();
                    string inr = "insert into rdls values(@r)";
                    SqlCommand inrc = new SqlCommand(inr, con);
                    inrc.Parameters.AddWithValue("@r", txtr.Value.ToString());
                    inrc.ExecuteNonQuery();
                    
                }
            }
            if (lbr.Value.ToString() != string.Empty)
            {
                lbr.Value = lbr.Value + ",\n" + txtr.Value.ToString();
                txtr.Value = "";
            }
            else
            {
                lbr.Value = txtr.Value.ToString();
                txtr.Value = "";
            }
            con.Close();
            getrdl();
        }

        protected void addf_Click(object sender, EventArgs e)
        {
            if (txtf.Value.ToString() != "")
            {
                string getf = "select func from functions where func=@f";
                SqlCommand cmd_getf = new SqlCommand(getf, con);
                cmd_getf.Parameters.AddWithValue("@f", txtf.Value.ToString());
                con.Open();
                cmd_getf.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_getf);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count < 1)
                {
                    ds.Clear();
                    string inf = "insert into functions values(@f)";
                    SqlCommand infc = new SqlCommand(inf, con);
                    infc.Parameters.AddWithValue("@f", txtf.Value.ToString());
                    infc.ExecuteNonQuery();
                    
                }
            }
            if (lbf.Value.ToString() != string.Empty)
            {
                lbf.Value = lbf.Value + ",\n" + txtf.Value.ToString();
                txtf.Value = "";
            }
            else
            {
                lbf.Value = txtf.Value.ToString();
                txtf.Value = "";
            }
            con.Close();
            getfun();
        }
    }
}