﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User Page.aspx.cs" Inherits="Matrix_Team_Worked_Pages_and_Procedures.User_Page" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Page</title>
    <link rel="shortcut icon" type="image/x-icon" href="\icon.ico"/>
</head>
<body style="background-image:url(download.jpg);background-position: center center;background-repeat: no-repeat;background-attachment: fixed;background-size:cover;height:100%;width:100%;">
    <style>
         .butto 
        {
            background-color: #e600ac;
            color: white;
            padding: 10px 15px;margin-left:auto;margin-right:auto;display:block;margin-bottom:0%;            
            border: none;                 
            opacity: 0.9;               
        }
         .butto:hover 
        {
            background-color:white ;
            color: #e600ac;
            padding: 10px 15px;margin-left:auto;margin-right:auto;display:block;margin-bottom:0%;            
            border: none;
            cursor: pointer;            
            opacity: 0.9;               
        }
         
         h1,h2{color:#66ff33;}
         textarea{width:211px;height:100px;resize:none;}
         .buttoo
        {
            background-color: #e600ac;
            color: white;
            padding: 10px 15px;
            margin-left:632px;
            border: none;           
            width:50px;            
            opacity: 0.9;
            text-decoration:none;
        }
         .buttoo:hover
        {
            background-color:white ;
            color: #e600ac;
            padding: 10px 15px;
            margin-left:632px;
            border: none;
            cursor: pointer;
            width:50px;            
            opacity: 0.9;
            text-decoration:none;
        }
    </style>
    <form id="form1" runat="server">
        <div>
            <h1 align="center">User Page</h1>
            <table align="center">
                <tr>
                    <td>
                        <h2>Task No</h2>
                    </td>
                    <td>
                        <input type="text" list="taskNo" runat="server" id="txttno" required="required" placeholder="Task Number" autofocus="autofocus"/><asp:Button runat="server" OnClick="tno_Click" Text="Get"/>
                        <datalist id="taskNo" runat="server"/>          
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>Task Name</h2>
                    </td>
                    <td>
                        <input type="text" runat="server" id="txttna" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>Description</h2>
                    </td>
                    <td>
                        <input type="text" runat="server" id="txtd" readonly="readonly"/>                
                    </td>
                </tr>
            </table>
            <table align="center">
                <tr>
                    <td>
                        <h2>Pages</h2>
                    </td>
                    <td>
                        <input id="txtp" runat="server" list="lstp"/><asp:Button ID="addp" runat="server" Text="ADD" OnClick="addp_Click"/><br />
                        <textarea id="lbp" runat="server"/><datalist id="lstp" runat="server"/>
                        <asp:Label Text="&nbsp&nbsp&nbsp&nbsp" runat="server" /> 
                    </td>
                    <td>
                        <h2>Stored Procedures</h2>
                    </td>
                    <td>
                        <input id="txtsp" runat="server" list="lstsp"/><asp:Button ID="addsp" runat="server" Text="ADD" OnClick="addsp_Click"/><br />
                        <textarea id="lbsp" runat="server"/><datalist id="lstsp" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>Tables</h2>
                    </td>
                    <td>
                        <input id="txtt" runat="server" list="lstt"/><asp:Button ID="addt" runat="server" Text="ADD" OnClick="addt_Click"/><br />
                        <textarea id="lbt" runat="server"/><datalist id="lstt" runat="server"/>
                    </td>
                    <td>
                        <h2>Queries</h2>
                    </td>
                    <td>
                        <input id="txtq" runat="server" list="lstq"/><asp:Button ID="addq" runat="server" Text="ADD" OnClick="addq_Click"/><br />
                        <textarea id="lbq" runat="server"/><datalist id="lstq" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>RDLs</h2>
                    </td>
                    <td>
                        <input id="txtr" runat="server" list="lstr"/><asp:Button ID="addr" runat="server" Text="ADD" OnClick="addr_Click"/><br />
                        <textarea id="lbr" runat="server"/><datalist id="lstr" runat="server"/>
                    </td>
                    <td>
                        <h2>Functions</h2>
                    </td>
                    <td>
                        <input id="txtf" runat="server" list="lstf"/><asp:Button ID="addf" runat="server" Text="ADD" OnClick="addf_Click"/><br />
                        <textarea id="lbf" runat="server"/><datalist id="lstf" runat="server"/>
                    </td>
                </tr>
            </table>
            <asp:Button ID="Update" Text="Update" runat="server" CssClass="butto" OnClick="Update_Click"/><br />
            <asp:LinkButton ID="logout" Text="Log Out" runat="server" OnClick="logout_Click" CssClass="buttoo" OnClientClick="Confirm()"/>
            <script type = "text/javascript">
                function Confirm() {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    if (confirm("Are You Sure?")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                    document.forms[0].appendChild(confirm_value);
                }
            </script>   
        </div>
    </form>
</body>
</html>