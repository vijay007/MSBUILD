﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login Page.aspx.cs" Inherits="Matrix_Team_Worked_Pages_and_Procedures.Login_Page" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
    <link rel="shortcut icon" type="image/x-icon" href="\icon.ico"/>
</head>
<body style="background-image:url(download.jpg);background-position: center center;background-repeat: no-repeat;background-attachment: fixed;background-size:cover;height:100%;width:100%;background-color: #464646;">
    <style>
       .butto 
        {
            background-color:#e600ac;
            color: white;
            padding: 10px 15px;
            margin-left:auto;margin-right:auto;display:block;margin-bottom:0%;
            border: none;font-weight:bold;
            opacity: 0.9;
        }
       .butto:hover
        {
            background-color:white;
            color: #e600ac;
            padding: 10px 15px;
            margin-left:auto;margin-right:auto;display:block;margin-bottom:0%;
            border: none;font-weight:bold;
            cursor:pointer;
            opacity: 0.9;
        }
       input[type=text]:hover,input[type=password]:hover
        {
            background-color:#ebedf3;
            color: red;
        }h2{color:#66ff33;font-size:20px;}h1{color:#66ff33;}
    </style>
    <form id="form1" runat="server">
    <div>
        <br /><br /><br />
    <h1 align="center">Login Page</h1>
    <table align="center">
        <tr>
            <td>
                <h2>Name</h2>
            </td>
            <td>
                <input id="txtid" type="text" runat="server" placeholder="Enter your ID" required="required" autofocus="autofocus"/>
            </td>
        </tr>
        <tr>
            <td>
                <h2>Password</h2>
            </td>
            <td>
                <input type="Password" runat="server" placeholder="Enter your Password" id="txtpass" required="required"/>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="submit" runat="server" Text="LOGIN" CssClass="butto" OnClick="submit_Click"/>
    </div>
    </form>
</body>
</html>