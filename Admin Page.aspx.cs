﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Matrix_Team_Worked_Pages_and_Procedures
{
    public partial class Admin_Page : System.Web.UI.Page
    {
        public static string con_string ="Data Source=192.168.2.16;Initial Catalog=karthikeyan;Persist Security Info=True;User ID=sa;Password=Sql@123";
        SqlConnection con = new SqlConnection(con_string);
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string uname = Session["Name"].ToString();
                if (!IsPostBack)
                {                    
                    gettaskno();                    
                }
            }
            catch (NullReferenceException) { Response.Write("<script>if(!alert('Please Login with your details'))document.location='Login Page.aspx';</script>"); }            
        }

        private void gettaskno()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select TaskNo from contents", con);
            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                taskNo.InnerHtml = taskNo.InnerHtml + System.Environment.NewLine +
                    String.Format("<option value='{0}'>", ds.Tables[0].Rows[i][0]);
            }
            con.Close();
        }
        
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            Label lbldeleteid = (Label)row.FindControl("lblrow");            
            con.Open();
            SqlCommand cmd = new SqlCommand("delete FROM contents where row='" + GridView1.DataKeys[e.RowIndex].Value + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();
            Response.Write("<script>if(!alert('Deleted Successfully!!'))document.location='Admin Page.aspx';</script>");
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;            
        }        

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string r = GridView1.SelectedRow.Cells[0].Text;
                string query_get = "select row,Date=convert (varchar,[Date],100),TaskNo,TaskName,Description,status,Pages,Stored_Procedures,Tables,Queries,Rdls,Functions,Name from contents where row=@r";
                SqlCommand cmd_get = new SqlCommand(query_get, con);
                con.Open();
                cmd_get.Parameters.AddWithValue("@r", r);
                cmd_get.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd_get);
                da.Fill(ds);
                GridView1.DataSource = ds;
                GridView1.DataBind();
                SqlDataReader sdr = cmd_get.ExecuteReader();
                sdr.Read();
                string tno = sdr[2].ToString(); txttno.Value = tno;
                string tna = sdr[3].ToString(); txttna.Value = tna;
                string td = sdr[4].ToString(); txtd.Value = td;
                string status = sdr[5].ToString(); ddlstatus.SelectedValue = status;
                sdr.Close();
                con.Close();
            }
            catch (Exception) { }
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            string query_btn = "select TaskNo from contents where TaskNo=@inp";
            SqlCommand cmd_get = new SqlCommand(query_btn, con);
            con.Open();
            cmd_get.Parameters.AddWithValue("@inp", txttno.Value.ToString());
            cmd_get.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd_get);
            da.Fill(ds);
            if(ds.Tables.Count==1 && ds.Tables[0].Rows.Count>0)
            {
                if (txttno.Value.ToString() != string.Empty && txttna.Value.ToString() != string.Empty && ddlstatus.SelectedValue != "" || txtd.Value.ToString() != string.Empty)
                {
                    string upquery = "update contents set status=@s,TaskName=@tna,Description=@des where TaskNo=@tno";
                    SqlCommand upcmd = new SqlCommand(upquery, con);
                    upcmd.Parameters.AddWithValue("@tno", txttno.Value.ToString());
                    upcmd.Parameters.AddWithValue("@tna", txttna.Value.ToString());
                    upcmd.Parameters.AddWithValue("@des", txtd.Value.ToString());
                    if (ddlstatus.SelectedValue == "yes")
                    {
                        upcmd.Parameters.AddWithValue("@s", "yes");
                    }
                    else
                    {
                        upcmd.Parameters.AddWithValue("@s", "no");
                    }
                    upcmd.ExecuteNonQuery();
                }
                string query = "select row,Date=convert (varchar,[Date],100),TaskNo,TaskName,Description,status,Pages,Stored_Procedures,Tables,Queries,Rdls,Functions,Name from contents where TaskNo=@tno";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@tno", txttno.Value.ToString());
                cmd.ExecuteNonQuery();
                DataSet ds1 = new DataSet();
                SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                da1.Fill(ds1);
                GridView1.DataSource = ds1;
                GridView1.DataBind();
                SqlDataReader sdr = cmd.ExecuteReader();
                sdr.Read();
                string tno = sdr[2].ToString(); txttno.Value = tno;
                string tna = sdr[3].ToString(); txttna.Value = tna;
                string td = sdr[4].ToString(); txtd.Value = td;
                string status = sdr[5].ToString(); if (status == "yes") ddlstatus.SelectedValue = "yes"; else ddlstatus.SelectedValue = "no";
                sdr.Close();                             
                con.Close();
            }
            else
            {
                if (txttno.Value.ToString() != string.Empty && txttna.Value.ToString() != string.Empty && ddlstatus.SelectedValue!="")
                {
                    string queryins = "insert into contents(TaskNo,TaskName,Description,status)values(@tno,@tna,@d,@s)";
                    SqlCommand cmdins = new SqlCommand(queryins, con);                                       
                    cmdins.Parameters.AddWithValue("@tno", txttno.Value.ToString());
                    cmdins.Parameters.AddWithValue("@tna", txttna.Value.ToString());
                    cmdins.Parameters.AddWithValue("@d", txtd.Value.ToString());
                    cmdins.Parameters.AddWithValue("@s", ddlstatus.SelectedValue);
                    cmdins.ExecuteNonQuery();
                    string query = "select row,Date=convert (varchar,[Date],100),TaskNo,TaskName,Description,status,Pages,Stored_Procedures,Tables,Queries,Rdls,Functions,Name from contents where TaskNo=@tno";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@tno", txttno.Value.ToString());
                    cmd.ExecuteNonQuery();
                    DataSet ds1 = new DataSet();
                    SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                    da1.Fill(ds1);
                    GridView1.DataSource = ds1;
                    GridView1.DataBind();
                    con.Close();
                }
                else
                {
                    Response.Write("<script>if(!alert('Please fill TaskNo, TaskName and select IsCompleted Option'))document.location='Admin Page.aspx';</script>");
                }
            }
        }

        protected void lbtnno_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select row,Date=convert (varchar,[Date],100),TaskNo,TaskName,Description,status,Pages,Stored_Procedures,Tables,Queries,Rdls,Functions,Name from contents where status='no' order by Date asc";
            SqlCommand cmd = new SqlCommand(query, con);            
            cmd.ExecuteNonQuery();
            DataSet ds1 = new DataSet();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd);
            da1.Fill(ds1);
            GridView1.DataSource = ds1;
            GridView1.DataBind();
            txttno.Value = "";
            txttna.Value = "";
            txtd.Value = "";
            ddlstatus.SelectedValue = "";
            
        }

        protected void lbtnyes_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "select row,Date=convert (varchar,[Date],100),TaskNo,TaskName,Description,status,Pages,Stored_Procedures,Tables,Queries,Rdls,Functions,Name from contents where status='yes'";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            DataSet ds1 = new DataSet();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd);
            da1.Fill(ds1);
            GridView1.DataSource = ds1;
            GridView1.DataBind();
            txttno.Value = "";
            txttna.Value = "";
            txtd.Value = "";
            ddlstatus.SelectedValue = "";
        }

        protected void clear_Click(object sender, EventArgs e)
        {
            txttno.Value = "";
            txttna.Value = "";
            txtd.Value = "";
            ddlstatus.SelectedValue = "";
            GridView1.DataSource = null;
            GridView1.DataBind();     
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                Session["Name"] = null;
                Response.Redirect("Login Page.aspx");
            }
        }
    }
}