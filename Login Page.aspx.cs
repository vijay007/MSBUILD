﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;

namespace Matrix_Team_Worked_Pages_and_Procedures
{
    public partial class Login_Page : System.Web.UI.Page
    {
        public static string con_string = "Data Source=192.168.2.16;Initial Catalog=karthikeyan;Persist Security Info=True;User ID=sa;Password=Sql@123";
        SqlConnection con = new SqlConnection(con_string);
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {           
                        con.Open();
                        string query = "select Password,Type from matrixusers where Name=@id";
                        SqlCommand cmd = new SqlCommand(query, con);
                        cmd.Parameters.AddWithValue("@id", txtid.Value.ToString());
                        cmd.ExecuteNonQuery();
                        DataSet ds = new DataSet();
                        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                        sda.Fill(ds);
                        if (ds.Tables[0].Rows.Count == 1)
                        {
                            SqlDataReader sdr = cmd.ExecuteReader();
                            sdr.Read();
                            string pass = sdr[0].ToString();
                            string type = sdr[1].ToString();
                            sdr.Close();
                            if (pass == txtpass.Value.ToString())
                            {
                                Session["txtid"] = txtid.Value.ToString();                                
                                if (type == "admin")
                                {
                                    Session["Name"] = txtid.Value.ToString();
                                    Response.Write("<script>if(!alert('Logged In Successfully!!!'))document.location='Admin Page.aspx';</script>");
                                }
                                else
                                {
                                    Session["Name"] = txtid.Value.ToString();
                                    Response.Write("<script>if(!alert('Logged In Successfully!!!'))document.location='User Page.aspx';</script>");
                                }
                            }
                            else
                            {                                
                                Response.Write("<script>alert('Password Wrong.')</script>");
                                txtpass.Value = "";
                                txtpass.Focus();
                            }                            
                        }
                        else
                        {
                            Response.Write("<script>if(!alert('You are not a Member of Matrix Team.So,Please Contact Matrix Team Head to Register Your details'))document.location='Login Page.aspx';</script>");                            
                        }                                                   
            }
            catch (Exception) { }
            con.Close();
        }
    }
}